<div class="sidebar-nav">
<ul>
<?php if($_SESSION["tipo_usuario"]!=2){ ?>
<li><a href="principal.php" class="nav-header"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-edit"></i> Consultas<i class="fa fa-collapse"></i></a></li>
<li>
  <ul class="legal-menu nav nav-list collapse">
    <li><a href="liquidar_empleados.php"><span class="fa fa-caret-right"></span> Cantidad de Niveles</a></li>
    <li><a href="liquidaciones.php"><span class="fa fa-caret-right"></span> Consulta 2</a></li>  
    <li><a href="conceptos.php"><span class="fa fa-caret-right"></span> Consulta 3</a></li>  
  </ul>
</li>   


<li><a href="departamentos.php" class="nav-header"><i class="fa fa-book"></i> Departamentos</a></li>
<li><a href="empleados.php" class="nav-header"><i class="fa fa-users"></i> Empleados</a></li>


<?php } else { ?>
<li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-shopping-cart"></i> Ventas<i class="fa fa-collapse"></i></a></li>
<li>
  <ul class="dashboard-menu nav nav-list collapse in">
    <li><a href="venta_nueva.php"><span class="fa fa-caret-right"></span> Crear Venta</a></li>
  </ul>
</li>
<?php } ?>
 </div> 

 